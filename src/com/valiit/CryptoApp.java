package com.valiit;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CryptoApp {

    private static final String ALPHABET_LOCATION = "resources/alfabeet.txt";

    public static void main(String[] args) throws IOException {
        List<String> alphabetLines = readFileLines(ALPHABET_LOCATION);
        Map<String, String> alphabet = generateCryptoMap(alphabetLines);

        String encryptedMessage = encryptString(args[0], alphabet);
        String decryptedMessage = decryptString(encryptedMessage, alphabet);

        System.out.printf("Krüpteerimist vajav sõnum: %s", args[0]);
        System.out.println();
        System.out.printf("Krüpteeritud sõnum: %s", encryptedMessage);
        System.out.println();
        System.out.printf("Dekrüpteeritud sõnum: %s", decryptedMessage);
    }

    private static List<String> readFileLines(String fileLocation) throws IOException {
        Path path = Paths.get(fileLocation);
        return Files.readAllLines(path);
    }

    private static Map<String, String> generateCryptoMap(List<String> lines) {
        Map<String, String> dictionary = new HashMap<>();
        for (String line : lines) {
            String[] lineParts = line.split(", ");
            dictionary.put(lineParts[0], lineParts[1]);
        }
        return dictionary;
    }

    private static String encryptString(String text, Map<String, String> alphabet) {
        String result = "";
        for (char character : text.toCharArray()) {
            result += encryptChar(character, alphabet);
        }
        return result;
    }

    private static String decryptString(String text, Map<String, String> alphabet) {
        String result = "";
        for (char character : text.toCharArray()) {
            result += decryptChar(character, alphabet);
        }
        return result;
    }

    private static char encryptChar(char value, Map<String, String> alphabet) {
        if (alphabet.containsKey(String.valueOf(value))) {
            return alphabet.get(String.valueOf(value)).charAt(0);
        }
        return value;
    }

    private static char decryptChar(char value, Map<String, String> alphabet) {
        for (Map.Entry entry : alphabet.entrySet()) {
            if (((String) entry.getValue()).charAt(0) == value) {
                return ((String) entry.getKey()).charAt(0);
            }
        }
        return value;
    }
}
